package com.example.mapexample

import android.app.Application
import android.content.Context

/**
 *   Created by Keshav Bhusal on 7/2/20
 */
class MyApplication : Application() {
    companion object {
         lateinit var context: Context
    }

    override fun onCreate() {
        super.onCreate()
        context = applicationContext
    }
}