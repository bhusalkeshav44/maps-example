package com.example.mapexample.raw

import com.example.mapexample.R
import com.example.mapexample.model.RestaurantsModel

/**
 *   Created by Keshav Bhusal on 7/2/20
 */
class DataCollection {

    companion object {
        fun restaurantData(): List<RestaurantsModel> {
            val arrayList = ArrayList<RestaurantsModel>()
            val model = RestaurantsModel(
                "Kathmandu Steak House Restaurant",
                "Kathmandu",
                27.713776,
                85.310239,
                R.drawable.ic_bakery
            )
            val model1 = RestaurantsModel(
                "Kathmandu Grill Restaurant & bar",
                "Kathmandu",
                27.718853,
                85.312291,
                R.drawable.ic_shop
            )
            val model2 = RestaurantsModel(
                "Rosemary Kitchen And Coffee Shop",
                "Kathmandu",
                27.712641,
                85.311435,
                R.drawable.ic_bakery
            )
            val model3 = RestaurantsModel(
                "Fusion Himalaya café & restaurant",
                "Kathmandu",
                27.716200,
                85.308564,
                R.drawable.ic_shop
            )
            val model4 = RestaurantsModel(
                "Forest & Plate",
                "Kathmandu",
                27.715374,
                85.310962,
                R.drawable.ic_bakery
            )
            arrayList.add(model)
            arrayList.add(model1)
            arrayList.add(model2)
            arrayList.add(model3)
            arrayList.add(model4)

            return arrayList
        }
    }


}