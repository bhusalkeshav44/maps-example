package com.example.mapexample.model

import com.example.mapexample.R

/**
 *   Created by Keshav Bhusal on 7/2/20
 */
data class RestaurantsModel(
    var name: String = "",
    var address: String = "",
    var lat: Double = 0.0,
    var lon: Double = 0.0,
    var icon: Int = R.drawable.ic_baseline_emoji_people_24
)