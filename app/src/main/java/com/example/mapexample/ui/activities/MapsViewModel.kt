package com.example.mapexample.ui.activities

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mapexample.model.RestaurantsModel
import com.example.mapexample.raw.DataCollection

/**
 *   Created by Keshav Bhusal on 7/2/20
 */
class MapsViewModel : ViewModel() {
    val mapsData = MutableLiveData<List<RestaurantsModel>>()

    init {
        setRestaurantData()
    }

    private fun setRestaurantData() {
        mapsData.postValue(DataCollection.restaurantData())
    }
}