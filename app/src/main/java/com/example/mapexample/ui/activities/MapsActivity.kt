package com.example.mapexample.ui.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.mapexample.R
import com.example.mapexample.utils.CommonHelper

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

private const val TAG = "MapsActivity"

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private val viewModel: MapsViewModel by lazy {
        ViewModelProvider(this).get(MapsViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        // Add a marker in pulchowk and move the camera

        val pulchowk = LatLng(27.713946,85.308480)
        mMap.addMarker(
            MarkerOptions().position(pulchowk).title("My Position").icon(
                CommonHelper.bitmapDescriptorFromVector(R.drawable.ic_baseline_emoji_people_24)
            )
        )
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(pulchowk, 17f))

        viewModel.mapsData.observe(this, Observer { response ->
            response.forEach { item ->
                val myLatLng = LatLng(item.lat, item.lon)
                mMap.addMarker(
                    MarkerOptions().position(myLatLng).title(item.name).icon(
                        CommonHelper.bitmapDescriptorFromVector(item.icon)
                    ).snippet(item.address)
                )
                mMap.moveCamera(CameraUpdateFactory.newLatLng(myLatLng))

            }


        })


    }
}